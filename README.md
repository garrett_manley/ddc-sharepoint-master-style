# DDC SharePoint Master Style

- [DDC SharePoint Master Style](#ddc-sharepoint-master-style)
  - [What is this](#what-is-this)
  - [How to style SharePoint pages](#how-to-style-sharepoint-pages)
    - [Quick Start](#quick-start)
  - [How to contribute](#how-to-contribute)
  - [Who to talk to](#who-to-talk-to)

## What is this

* This project manages the scss library implemented in the creation of pages for the DDC SharePoint site
* Version: 2.0.1

## How to style SharePoint pages

Using this style in a SharePoint page is fairly easy.
Follow this quick start guide to get up and running.
Below will be general style guidelines to follow to ensure a
consistent look and feel.

### Quick Start

1. On a new or existing blank SharePoint page, open the page editor.
2. In the editor ribbon, select the insert tab.
3. Select "Embed Code" for what you would like to insert
4. In the Embed text box, insert the following code: `<link rel="stylesheet" href="https://hub.perficient.com/bu/DDCv2/Style%20Library/Custom%20CSS/main.min.css">`
5. Select Insert and your page will be ready to be styled using the CSS that is located in the directory location listed above.

## How to contribute

Get a clone of the [repositroy](https://garrett_manley@bitbucket.org/garrett_manley/ddc-sharepoint-master-style) and create a branch to work on.

``` git
$ git clone https://garrett_manley@bitbucket.org/garrett_manley/ddc-sharepoint-master-style.git

```

When you've completed your work, create a mergre request and it will be reviewed and approved by whoever is in charge of the project, currently Garrett Manley - garrett.manley@perficient.com

## Who to talk to

* Originally developed by Garrett Manley - garrett.manley@perficient.com